import * as d3 from 'd3';   

const data = [
    {
        date: '2013-01',
        value: 10
    },
    {
        date: '2013-02',
        value: 15
    },
    {
        date: '2013-03',
        value: 30
    },
    {
        date: '2013-04',
        value: 35
    }
  ];

export default function chart2 () {

    const margin = {top: 20, right: 20, bottom: 80, left: 100};
    const innerWidth = 600 - margin.left - margin.right;
    const innerHeight = 300 - margin.top - margin.bottom;

    const svg = d3.select('.chart2')
                        .append('svg')
                        .attr('class', 'container');

    const xValue = d => d.date;

    const yValue = d => d.value;
    
    const xScale = d3
                    .scaleBand()
                    .domain(data.map(xValue))
                    .rangeRound([0, innerWidth])
                    .padding(0.1);
    const yScale = d3
                    .scaleLinear()
                    .domain([0, d3.max(data, yValue )])
                    .range([innerHeight, 0]);

    const g = svg.append('g')
    .attr('transform', `translate(${margin.left}, ${margin.top})`);

    g.append('g')
        .call(d3.axisLeft(yScale))
        .selectAll('.domain, .tick line')
        .remove();

    g.append('g')
        .call(d3.axisBottom(xScale))
        .attr('transform', `translate(0, ${innerHeight})`);
    
    g.selectAll('.bar')
        .data(data)
        .enter()
        .append('rect')
        .attr('class', 'bar')
        .attr('width', xScale.bandwidth())
        .attr('height', data => (200 - yScale(data.value)))
        .attr('x', data => xScale(data.date))
        .attr('y', data => yScale(data.value));

}


