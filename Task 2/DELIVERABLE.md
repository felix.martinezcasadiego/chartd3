# Deliverable

- First, open the document in `\Tasks-est-main\app`
- Create component in `app/src/components/button/Button.vue`
- I added the button component to `app/App.vue`
- Verify in package.json the existence of sass and sass-loader in the dependencies, I didn't see any sass dependency
- Proceed to download sass as `npm install --save-dev node-sass sass-loader`
- Created a scss file in `app/src/scss/main.scss` 
- Then I added to the `app/App.vue` the `<style lang='scss' src="./scss/main.scss"></style>` code
- Added a style for the body in `app/src/scss/main.scss`, then I ran a test without getting any errors
- I created a reusable button component in `app/src/components/button/Button.vue`
- I created an scss file in `src/scss/scss/button/button.scss` to style the button with dimensions, colors, hoover and text decoration. The `main.scss` is used as a container for all the scss in the project
- Then I returned to proceeded to conditional rendering, where I changed the component to render an `a` if we receive an `href` prop
- I created a dynamic component `<component> <slot/> </component>`, with the logic that if there is nothing in `href` then return a `button`, otherwise return an `a`
