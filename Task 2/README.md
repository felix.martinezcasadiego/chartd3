# Task 2

Consists on building a simple and reusable Vue component.

Please read instructions below.

## Main Focus

- Vue.
- SCSS.
- Style methodology.

## Task

Create a simple standalone component that could be reused in several instances, with some dynamic properties to customize but also with low dependency.

Examples:

- Button.
- Carousel.
- Modal.
- Form/form element.
- ...

## Deliverable

- Create the required component in `app/src/components/` folder or create a subfolder if you find it appropriate.
- Create any global scss file, if you find it appropriate.
- Add the component in the main `app/App.vue`. It should be visible on first load once the app is served.
- Explain in the DELIVERABLE.md file the component built and any other considerations.

## Review criteria

These are the following criteria that will be used to evaluate the task:

1. *Best practices:* Component structure, should be clean, simple and easily readable.
2. *Functionality:* The implicit functionality should work as expected.
3. *Reusability:* Low dependency on other components, i.e. easy to reuse in other projects.
4. *Styling:* SCSS usage and methodology used, if any.