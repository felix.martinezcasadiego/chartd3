# Task 1

Please read instructions below.

## Languages

- Node.

## Installation

``` powershell
npm i
```

## Execution

``` powershell
npm run serve
```

## Task

Make the build system work as expected.

## Deliverable

- Vue app should build as expected.
- Vue app should be able to be served locally.
- Please explain in DELIVERABLE.md the work done.

## Review criteria

These are the following criteria that will be used to evaluate the task:

1. *Build system:* Install and serve the app as expected.
