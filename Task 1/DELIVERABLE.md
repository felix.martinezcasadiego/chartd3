# Deliverable

- First, open the document in `\Tasks-est-main\app`
- Verify package.json and verify the downloaded scripts 
- Open powershell and download npm as 
``` powershell
npm i
```
- Verify the operation of the app in serve as 
``` powershell
npm run serve
```
- Verify the operation of the app in serve as
``` powershell
npm run build
```
- The build system work as expected
