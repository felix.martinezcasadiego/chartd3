# Deliverable

- I Created component in `app/src/components/api/api.vue`
- I added the component `api.vue` to `app/App.vue`
- Consume an API using `fetch` with `async`, `await`, and `created()` as lifecycle
- I used `this` for access to the `posts` variable, in this case, what I do is give value to one of the variables of the data
- Then I did the layout of the API rendering in the app, using `v-for` directive to loop over data
- Created a scss file in `app/src/scss/table/table.scss`
- Then I put a function on the button using the `v-on.click.native` directive to catch and handle events, then I used `data` function to return the `isShow` variable as `false`, and added in the `<api></api>` component the `v-show="isShow"` directive as a toggle the display. So when the user clicks on the button can show or hide the API