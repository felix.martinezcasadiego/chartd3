# Task 3

Consists on building a Vue component that interacts with an API.

Please read instructions below.

## Main Focus

- Vue.
- Fetch.

## Task

Create a component that interacts with en external public API and has a COOL ;) functionality.

Examples:

- Country dropdown.
- Quotes list.
- ...

## Deliverable

- Create the required component in `app/src/components/` folder or create a subfolder if you find it appropriate.
- Add the component in the main `app/App.vue`.
- Explain in the DELIVERABLE.md file the component built and any other considerations.

## Review criteria

These are the following criteria that will be used to evaluate the task:

1. *Best practices:* Component structure, should be clean, simple and easily readable.
2. *API management:* Get and process data.
3. *Functionality:* Build/Execution creativity.
