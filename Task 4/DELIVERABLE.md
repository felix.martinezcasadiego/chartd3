# Deliverable

- Refactor the file in `app/src/core/chart2.js`
- Verify in package.json that `d3` existence, the dependencies existed but, I downloaded and installed the app the `npm i d3` to have the most up-to-date one
- Then I checked the existing `chart` component
- I Created component in `app/src/core/chart2.js`
- I import d3 as `import * as d3 from 'd3`
- I keep the same `svg` variable
- I keep the variable `margin` and change the names of the variables `width` and `height` to `innerWidth` and `innerWidth` and convert them to const variables, because for me it's more understandable that way
- I used the `svg` variable to make the chart with the data, using `selectAll()` function to select all the element that matches the specified selector string, then `data()` function to take the data values, the `enter()` to create the missing elements and return the enter selection, the `append()` function for creating a rectangle, and `attr()` functions to define the position of chart bars
- Created a scss file in `app/src/scss/chart/chart.scss` and `app/src/scss/font/font.scss` for all letters of the project
- I created the variable `xScale` to manipulate the x-axis, using `scaleBand()` for give a domain of a certain length and divide the range, then used `domain()` to establish the domain of the scale using the `map()` method to take the data 
- Then I created `yScale` to manipulate the y-axis with the `scaleLinear()` To construct a linear scale. The `domain()` function then represents the minimum and max values for the domain
- I created two variables `xValue` for the x-axis and `yValue` for the y-axis. This because it is a repetitive code and to facilitate the reading of the code
- Then I created `g` variable  to group SVG shapes together, and changed `svg.selectAll('.bar')..` to `g.selectAll('.bar')...`
- Then I used the `axisLeft()` function to create a left vertical axis and remove `.domain` and `.tick line` to improve the chart
- I used the `axisBottom()` function to create a bottom horizontal axis and the `attr` function to move the horizontal axis in a correct way
- I modified the values of the `margin` variable to improve the chart view
- Created a scss file in `app/src/scss/card/card.scss` to create new styles for the entire element as a card
