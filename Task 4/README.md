# Task 4

JavaScript file refactor and simple update.

Please read instructions below.

## Prerequisites

- Node v12.18.0.
- npm.

## Main Focus

- JavaScript.
- D3.

## Task

Refactor the chart file using modern JavaScript and update it so the chart looks nicely on the homepage app.

## Deliverable

- Refactor the file in `app/src/core/chart.js`.
- Update the style/code of the chart to look different from the proposed one.
- Explain in the DELIVERABLE.md file the changes done and any other considerations.

## Review criteria

These are the following criteria that will be used to evaluate the task:

1. *Best practices:* Modern JavaScript used.
2. *Functionality:* The bar chart should be displayed and look different from the initial version.
3. *Enhancements:* Feel free to add any enhancements to the chart, such as animations, interactions, etc.
