# Technical Test

The purpose of this test is to evaluate a different sort of technical skills.

Once the task is completed, please zip the files again and send them to huumun team.

## Description

The candidate should try to achieve all requirements from each task proposed in the test.

Each task has an individual folder and its suggested/recommended to follow the order proposed.

All tasks point to same application, i.e., `app/` and all tasks should be resolved there.

Each task will be evaluated individually.

## Prerequisites

- Node v12.18.0.
- npm.

## Languages & Frameworks

- Vue.
- CSS/SCSS.
- JavaScript.
- TypeScript.
- d3.js.

## Installation

Please run:

``` powershell
npm i
```

## Execution

Please run:

``` powershell
npm run serve
```

## Deliverable

Please, refer to each task README.md file.

